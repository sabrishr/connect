package edu.mcgraw.edu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class BookContentActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    public static final String INTENT_BOOK_IMAGE_ID = "intent_book_image_id";
    public static final String INTENT_BOOK_TITLE = "intent_book_title";
    public static final String INTENT_BOOK_DESCRIPTION = "intent_book_description";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_content);
        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        populateView();
    }

    private void populateView(){

        Intent intent = getIntent();
        if(intent != null && intent.getExtras() != null){

            int imageId = intent.getIntExtra(INTENT_BOOK_IMAGE_ID, -1);
            String book_title = intent.getStringExtra(INTENT_BOOK_TITLE);
            String bookDescription = intent.getStringExtra(INTENT_BOOK_DESCRIPTION);

            if(imageId > 0){
                ((ImageView)findViewById(R.id.book_icon)).setImageResource(imageId);
            }

            if(book_title != null){
                ((TextView)findViewById(R.id.book_title)).setText(book_title);
            }

            if(bookDescription != null){
                ((TextView)findViewById(R.id.book_list_item2)).setText(bookDescription);
            }
        }

        populateListView();
    }

    private void populateListView(){
        ListView listView = (ListView)findViewById(R.id.book_content_list);
        String[] listItems = new String[]{"Track your program", "Table of Contents", "Preface","Parameters for opening PDF Files"};
        ArrayAdapter arrayAdapter = new ArrayAdapter<String> (this, android.R.layout.simple_list_item_1, listItems);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Intent intent = new Intent(this, ReadBookActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
